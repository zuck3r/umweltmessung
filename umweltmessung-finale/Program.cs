﻿using System;
using System.Threading;
using System.IO;

namespace Umweltmessung
{
	class Program
	{
		public static void Main (string[] args)
		{
			// Deklaration (/Initialisierung) der Variablen.

			// Allg. StreamReader-Objekt
			StreamReader sr = new StreamReader ("..\\..\\..\\data\\src\\src.sr.txt");
			sr.Close ();

			// Allg. StreamWriter-Objekt
			StreamWriter sw = new StreamWriter ("..\\..\\..\\data\\src\\src.sw.txt");
			sw.Close ();

			// Niederschlagsmenge
			double[] nm = new double[366];

			// Temperatur
			double[] tp = new double[366];

			// Luftdruck
			double[] ld = new double[366];


			// Wertespektrum für Temperatur- und Niederschlagsmengen-Arrays
			// 6 = 6 x 2 Monate (0 => Januar/Februar; 1 => März/April etc.)
			// 2 = Min- bzw. Maxwert (0 => MinWert; 1 => MaxWert
			// z.B. verschiedeneWerteTp[0, 0] = Januar/Februar: -20°C (MinWert); verschiedeneWerteTp[0, 1] = Januar/Februar: 5°C (MaxWert)
			//							 J & F  |  M & A  | M & J |  J & A |  S & O |  N & D
			//							 min/max | min/max| min/max| min/max | min/max |min/max
			// Niederschlagsmengenspektrum
			//int[,] niederschlagsmengeSpektrum = new int[6, 2] { { 0, 60 }, { 0, 40 }, { 0, 30 }, { 0, 20 }, { 0, 45 }, { 0, 70 } };
			int[,] niederschlagsmengeSpektrum = new int[12, 2];

			// Laden von Spektrumdaten aus entsprechender Datei.
			sr = new StreamReader ("..\\..\\..\\data\\src\\src.niederschlagsmengeSpektrum.txt");
			loadSrcDataFromFile (niederschlagsmengeSpektrum, sr);
			sr.Close ();

			// Temperaturspektrum
			//int[,] temperaturSpektrum = new int[6, 2] { { -20, 5 }, { -5, 15 }, { 5, 25 }, { 15, 35 }, { 10, 20 }, { -10, 5 } };
			int[,] temperaturSpektrum = new int[12, 2];

			// Laden von Spektrumdaten aus entsprechender Datei.
			sr = new StreamReader ("..\\..\\..\\data\\src\\src.temperaturSpektrum.txt");
			loadSrcDataFromFile (temperaturSpektrum, sr);
			sr.Close ();


			// Tagespektrum falls die Eingabe dem eines Datums entspricht
			//int[] tagSpektrum = new int[12] { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
			int[] tagSpektrum = new int[12];

			// Laden der Daten des Tagesspektrums (Tage/Monat) aus Datei
			sr = new StreamReader ("..\\..\\..\\data\\src\\src.tagSpektrum.txt");
			for (int i = 0; i <= 11; i++) {
				tagSpektrum [i] = Convert.ToInt32 (sr.ReadLine ());
			}
			sr.Close ();


			// Allgemeine Eingabeveriable / Leerstelle (ja/nein) / aktuelle Datei / zu löschende Datei.
			string eingabe, leerstelle, aktuelleDatei = "", dateiZumLoeschen;

			// Integer-Variablen
			// allg. Werte-Variablen, allg. Tag-Variable, Update-Variablen
			int wert1 = 0, wert2 = 0, tag = 0, nmUpdate = 0, tpUpdate = 0, ldUpdate = 0;

			// bool-Variablen
			// checkDataAdministration für äußerste while-Schleife
			// CheckMain-Variable für äußere while-Schleife
			// checkLocal-Variable für alle inneren while-Schleifen
			// isDate zum Check, ob Eingabe Datumsformat hat
			// consoleClear zum Check, ob bei Menügenerierung Menü generiert werden soll (und Konsole gecleart werden soll)
			bool checkDataAdministration, checkMain = false, checkLocal = false, isDate = false, consoleClear = true;

			// Alle existierenden Dateinamen.
			string[] dateiNamen = getAllFiles ();



			// Start des eigentlichen Programms.
			Console.WriteLine ("Projekt Umweltmessung - Verwaltung aller Messdaten eines Jahres");
			Thread.Sleep (3000);


			checkDataAdministration = false;
			while (checkDataAdministration == false) {
				generateDataAdministrationMenue (ref consoleClear);

				eingabe = Console.ReadLine ();

				switch (eingabe) {
				// Neue Datei anlegen.
				case "1":

					Console.WriteLine ("Sie möchten eine neue Datei mit neuen Datensätzen anlegen.");
					Console.WriteLine ("Geben Sie der Datei bitte einen Namen.");

					checkLocal = false;
					while (checkLocal == false) {
						eingabe = Console.ReadLine ();

						// Modus verlassen und zurück zum Submenü.
						if (eingabe.ToLower () == "exit") {
							exit (2, ref checkMain, ref checkLocal);
							checkLocal = true;
							break;
						}

						// Allgemeine Überprüfung des eingegebenen Dateinamens
						if (eingabe.Length >= 16 | !IsValidFileName (eingabe)) {
							invalidValue ();
						}
						// wenn alles in Ordnung war, dann...
						else {
							// wenn Dateiname bereits exisiert
							if (!fileExist (eingabe)) {
								Console.WriteLine ();
								Console.WriteLine ("[Fehler: Dateiname existiert bereits]");
								Console.WriteLine ();
							}
							// wenn alles in Ordnung war, dann..
							else {
								// wird die Datei angelegt
								Thread.Sleep (500);
								Console.WriteLine ();
								Console.WriteLine ("[Datei \"" + eingabe + ".txt\" wird angelegt]");
								Console.WriteLine ("Bitte warten.");
								Thread.Sleep (1000);

								sw = new StreamWriter ("..\\..\\..\\data\\" + eingabe + ".txt");

								Console.WriteLine ("[Datei angelegt]");
								Thread.Sleep (1000);

								break;
							}
						}
					}

					if (checkLocal == false) {
						Console.WriteLine ();
						Thread.Sleep (500);
						Console.WriteLine ("[Zufällige Daten werden generiert]");
						Console.WriteLine ("Bitte warten.");
						Thread.Sleep (2500);

						// Aufruf der Funktion, um neue Daten zu generieren
						generateData (nm, tp, ld, niederschlagsmengeSpektrum, temperaturSpektrum);

						// Generierte Daten werden in Datei geschrieben
						for (int i = 0; i <= 365; i++) {
							sw.WriteLine (nm [i] + ", " + tp [i] + ", " + ld [i]);
						}

						sw.Flush ();
						sw.Close ();

						Console.WriteLine ("[Daten generiert]");
						Thread.Sleep (1000);
					}

					break;

				// Bestehende Datei benutzen / Daten laden.
				case "2":

					// Aufruf der Funktion, um alle existierenden Dateinamen zu erhalten
					dateiNamen = getAllFiles ();

					// Aufruf der Funktion, um alle existierenden Dateinamen auszugeben
					listAllFiles ();

					Console.WriteLine ();
					Console.WriteLine ("Welche Datei möchten Sie auswählen? [Index]");

					// Innere while-Schleife zur Fehlerüberprüfung (Index von Array dateiNamen)
					checkLocal = false;
					while (checkLocal == false) {
						eingabe = Console.ReadLine ();

						// Modus verlassen und zurück zum Submenü.
						if (eingabe.ToLower () == "exit") {
							exit (2, ref checkMain, ref checkLocal);
							checkLocal = true;
							break;
						}

						// Wenn Eingabe nicht numerisch ist
						if (!IsNumeric (eingabe)) {
							invalidValue ();
						} else {
							wert1 = Convert.ToInt32 (eingabe);

							// Wenn eingegebener Wert < 0 oder größer als Arraygröße ist
							if (wert1 < 0 | wert1 >= dateiNamen.Length) {
								invalidValue ();
							} else {
								// speichern des Namens der angeforderten Datei in aktuelleDatei
								aktuelleDatei = dateiNamen [wert1];

								Console.WriteLine ();
								Console.WriteLine ("[Daten von \"" + aktuelleDatei + "\" werden geladen]");
								Console.WriteLine ("Bitte warten.");

								// Laden aller Daten aus entsprechender Datei
								LoadDataFromFile (nm, tp, ld, aktuelleDatei, sr);

								Thread.Sleep (1500);
								Console.WriteLine ("[Daten geladen]");
								Thread.Sleep (1000);
								break;
							}
						}
					}

					if (checkLocal == true)
						checkMain = true;
					else
						checkMain = false;

					// Hauptmenü-Schleife
					while (checkMain == false) {
						generateMainMenue (ref consoleClear, aktuelleDatei);

						//Eingabe für Hauptmenüaktion.
						eingabe = Console.ReadLine ();

						switch (eingabe) {
						// Daten abfragen.
						case "1":

							// Innere while-Schleife für Funktionalität von Menüpunkt 1 (Daten abfragen).
							checkLocal = false;
							while (checkLocal == false) {
								// Generierung des Submenüs (Daten abfragen).
								generateSubMenueDataSelect (ref consoleClear, aktuelleDatei);

								// Eingabe für Submenüaktion.
								eingabe = Console.ReadLine ();

								switch (eingabe) {
								// Einzelnen Datensatz abfragen.
								case "1":

									Console.WriteLine ("Bitte einen Tag angeben, um die Daten zu diesem zu erhalten.");
									Console.WriteLine ("Mit \"exit\" verlassen Sie diesen Modus wieder.");

									// Innere while-Schleife zur Fehlerüberprüfung der Eingabe des Wertes (einzelner Tag).
									checkLocal = false;
									while (checkLocal == false) {
										eingabe = Console.ReadLine ();

										// Modus verlassen und zurück zum Submenü.
										if (eingabe.ToLower () == "exit") {
											exit (2, ref checkMain, ref checkLocal);
											break;
										}

										// Wenn Eingabe nicht numerisch ist und kein Datum vorliegt.
										if (!IsNumeric (eingabe) & !IsDate (eingabe, ref isDate)) {
											invalidValue ();
										} else {
											// Wenn eingegebener Wert numerisch ist.
											if (isDate == false) {
												wert1 = Convert.ToInt32 (eingabe);

												// Wert1 muss zwischen 1 und 365 sein.
												if (wert1 < 1 | wert1 > 365) {
													invalidValue ();
												} else {
													Console.WriteLine ("[Wert gespeichert]");
													Console.WriteLine ();

													// Aufruf der Funktion, um Daten aus den Arrays auszugeben.
													OutputDataFromArray (wert1, wert1, nm, tp, ld, tagSpektrum);
													Console.WriteLine ();
													Console.WriteLine ("Mit \"exit\" verlassen Sie diesen Modus.");
													Console.WriteLine ("Bitte einen neuen Tag angeben.");
												}
											}
											// Wenn eingegebener Wert dem eines Datums entspricht.
											else if (isDate == true) {
												// Ausrechnung von "wert1" anhand der Angabe eines Datums.
												wert1 = getDayValueFromDateString (eingabe, tagSpektrum);

												Console.WriteLine ("[Wert gespeichert]");
												Console.WriteLine ();

												// Aufruf der Funktion, um Daten aus den Arrays auszugeben.
												OutputDataFromArray (wert1, wert1, nm, tp, ld, tagSpektrum);
												Console.WriteLine ();
												Console.WriteLine ("Mit \"exit\" verlassen Sie diesen Modus.");
												Console.WriteLine ("Bitte einen neuen Tag angeben.");
											}
										}
									}

									break;

								// Zeitraum abfragen.
								case "2":

									Console.WriteLine ("Bitte geben Sie nacheinander zwei Tage an,");
									Console.WriteLine ("um die Daten in diesem Zeitraum zu erhalten.");
									Console.WriteLine ("Mit \"exit\" verlassen Sie diesen Modus wieder.");
									Console.WriteLine ();

									checkLocal = false;
									while (checkLocal == false) {

										// Innere while-Schleife zur Fehlerüberprüfung des eingegeben Wertes (Zeitraum) - erster Tag.
										checkLocal = false;
										while (checkLocal == false) {
											Thread.Sleep (500);
											Console.WriteLine ("Geben Sie den ersten Tag an.");
											eingabe = Console.ReadLine ();

											// Modus verlassen und zurück zum Submenü.
											if (eingabe.ToLower () == "exit") {
												exit (2, ref checkMain, ref checkLocal);
												checkLocal = true;
												break;
											}

											// Wenn Eingabe nicht numerisch ist und kein Datum vorliegt.
											if (!IsNumeric (eingabe) & !IsDate (eingabe, ref isDate)) {
												invalidValue ();
											} else {
												// Wenn eingegebener Wert numerisch ist.
												if (isDate == false) {
													wert1 = Convert.ToInt32 (eingabe);

													// Wert1 muss zwischen 1 und 365 sein.
													if (wert1 < 1 | wert1 > 365) {
														invalidValue ();
													} else {
														Console.WriteLine ("[Wert gespeichert]");
														Console.WriteLine ();
														break;
													}
												}
												// Wenn eingegebener Wert dem eines Datums entspricht.
												else if (isDate == true) {

													// Ausrechnung von "wert1" anhand der angabe eines Datums
													wert1 = getDayValueFromDateString (eingabe, tagSpektrum);

													Console.WriteLine ("[Wert gespeichert]");
													Console.WriteLine ();
													break;
												}
											}
										}

										// Zweiten Tag angeben.
										while (checkLocal == false) {
											Thread.Sleep (500);
											Console.WriteLine ("Geben Sie den zweiten Tag an.");
											eingabe = Console.ReadLine ();

											// Modus verlassen und zurück zum Submenü.
											if (eingabe.ToLower () == "exit") {
												exit (2, ref checkMain, ref checkLocal);
												checkLocal = true;
												break;
											}

											// Wenn Eingabe nicht numerisch ist und kein Datum vorliegt.
											if (!IsNumeric (eingabe) & !IsDate (eingabe, ref isDate)) {
												invalidValue ();
											} else {
												// Wenn eingegebener Wert numerisch ist.
												if (isDate == false) {
													wert2 = Convert.ToInt32 (eingabe);

													// Wert2 muss zwischen 1 und 365 sein.
													if (wert2 < 1 | wert2 > 365) {
														invalidValue ();
													}

													break;
												}
												// Wenn eingegebener Wert dem eines Datums entspricht.
												else if (isDate == true) {
													// Ausrechnung von "wert1" anhand der angabe eines Datums
													wert2 = getDayValueFromDateString (eingabe, tagSpektrum);

													break;
												}
											}
										}

										// Wenn wert2 < wert1 ist, werden die Werte der Variablen getauscht.
										if (wert2 < wert1) {
											swap (ref wert1, ref wert2);
										}

										if (checkLocal == false) {
											Console.WriteLine ("[Wert gespeichert]");
											Console.WriteLine ();

											// Abfrage, ob alle Messdaten oder nur der Mittelwert aus allen Messdaten im angegebenen
											// Zeitraum ausgegeben werden soll/en.
											averageOrAll ();
										}

										// Innere while-Schleife zur Fehlerüberprüfung des eingegeben Wertes (alle Werte oder nur Mittelwert).
										//checkLocal = false;
										while (checkLocal == false) {
											eingabe = Console.ReadLine ();

											// Wenn nur Mittelwert ausgegeben werden soll.
											if (eingabe.ToLower () == "m") {
												// Auswertungstext.
												evaluation ();

												// Funktion, um Mittelwert zu ermitteln und auszugeben.
												OutputAverageFromArray (wert1, wert2, nm, tp, ld, tagSpektrum);

												break;
											}
											// Wenn alle Messwerte (und Mittelwert) ausgegeben werden sollen.
											else if (eingabe.ToLower () == "all") {
												// Auswertungstext.
												evaluation ();

												// Funktion, um alle Messdaten im angegebenen Zeitraum auszugeben.
												OutputDataFromArray (wert1, wert2, nm, tp, ld, tagSpektrum);

												Console.WriteLine ();

												// Funktion, um Mittelwert zu berechnen und auszugeben.
												OutputAverageFromArray (wert1, wert2, nm, tp, ld, tagSpektrum);

												break;
											}
											//Wenn Eingabe weder "m" noch "all" war
											else {
												invalidValue ();
											}
										}

										if (checkLocal == false) {
											Console.WriteLine ();
											Console.WriteLine ("Mit \"exit\" verlassen Sie diesen Modus.");
										}
									}
									checkLocal = false;

									break;

								// Gesamtes Jahr abfragen.
								case "3":

									// Soll nur Mittelwert oder sollen alle Messwerte des Jahres
									// ausgegeben werden?
									averageOrAll ();

									// Innere while-Schleife zur Fehlerüberprüfung.
									checkLocal = false;
									while (checkLocal == false) {
										eingabe = Console.ReadLine ();

										// Wenn nur Mittelwert ausgegeben werden soll.
										if (eingabe.ToLower () == "m") {
											// Auswertungstext.
											evaluation ();

											// Funktion, um Mittelwert zu berechnen und auszugeben.
											OutputAverageFromArray (1, 365, nm, tp, ld, tagSpektrum);

											Console.WriteLine ();
											Console.WriteLine ("Drücken Sie eine beliebige Taste, um fortzufahren.");
											Console.ReadKey (true);

											break;
										}
										// Wenn alle Messdaten und der Mittelwert des Jahres ausgegeben werden sollen.
										else if (eingabe.ToLower () == "all") {
											// Auswertungstext
											evaluation ();

											// Funktion, um alle Messdaten des Jahres auszugeben.
											OutputDataFromArray (1, 365, nm, tp, ld, tagSpektrum);

											Console.WriteLine ();

											// Funktion, um Mittelwert des Jahres zu berechnen und auszugeben.
											OutputAverageFromArray (1, 365, nm, tp, ld, tagSpektrum);

											Console.WriteLine ();
											Console.WriteLine ("Drücken Sie eine beliebige Taste, um fortzufahren.");
											Console.ReadKey (true);

											break;
										}
										// Exit-Funktionalität (zurück zum Submenü)
										else if (eingabe.ToLower () == "exit") {
											exit (2, ref checkMain, ref checkLocal);
											break;
										}
										// Wenn Eingabe weder "m" noch "all" noch "exit" war.
										else {
											invalidValue ();
										}
									}

									break;

								// Modus verlassen / zurück zum Hauptmenü.
								case "4":

									exit (1, ref checkMain, ref checkLocal);

									break;

								// Modus verlassen / zurück zum Hauptmenü (Alternative zu "4").
								case "exit":

									exit (1, ref checkMain, ref checkLocal);

									break;

								// Falls ungültiger Wert eingegeben wurde.
								default:

									invalidValue ();
									consoleClear = false;

									break;
								}
							}

							break;

						// Daten bearbeiten.
						case "2":

							checkLocal = false;
							while (checkLocal == false) {
								// Generierung des Submenüs (Daten bearbeiten).
								generateSubMenueDataUpdate (ref consoleClear, aktuelleDatei);

								// Eingabe für Submenüaktion.
								eingabe = Console.ReadLine ();

								switch (eingabe) {
								case "1":

									Console.WriteLine ("Bitte einen Tag angeben, um die Daten zu diesem bearbeiten zu können.");
									Console.WriteLine ("Mit \"exit\" verlassen Sie diesen Modus wieder.");

									// Innere while-Schleife zur Fehlerüberprüfung der Eingabe des Wertes (einzelner Tag).
									checkLocal = false;
									while (checkLocal == false) {
										eingabe = Console.ReadLine ();

										// Modus verlassen und zurück zum Submenü.
										if (eingabe.ToLower () == "exit") {
											exit (2, ref checkMain, ref checkLocal);
											checkLocal = true;
											break;
										}

										// Wenn Eingabe nicht numerisch ist.
										if (!IsNumeric (eingabe) & !IsDate (eingabe, ref isDate)) {
											invalidValue ();
										} else {

											// Wenn eingegebener Wert numerisch ist.
											if (isDate == false) {
												tag = Convert.ToInt32 (eingabe);

												// Wert1 muss zwischen 1 und 365 sein.
												if (tag < 1 | tag > 365) {
													invalidValue ();
												} else
													break;
											}
											// Wenn eingegebener Wert dem eines Datums entspricht.
											else if (isDate == true) {
												// Ausrechnung von "wert1" anhand der angabe eines Datums
												tag = getDayValueFromDateString (eingabe, tagSpektrum);

												break;
											}
										}
									}

									if (checkLocal == false) {
										Console.WriteLine ("[Wert gespeichert]");
										Console.WriteLine ();

										// Funktion, um alle Messdaten des Jahres auszugeben.
										OutputDataFromArray (tag, tag, nm, tp, ld, tagSpektrum);

										Thread.Sleep (1000);
										Console.WriteLine ("Geben Sie nun nacheinander die neuen Werte für diesen Tag ein.");
										Console.WriteLine ();
										Console.WriteLine ("Neue Niederschlagsmenge:");
									}

									//checkLocal = false;
									while (checkLocal == false) {
										eingabe = Console.ReadLine ();

										// Modus verlassen und zurück zum Submenü.
										if (eingabe.ToLower () == "exit") {
											exit (2, ref checkMain, ref checkLocal);
											checkLocal = true;
											break;
										}

										// Wenn Eingabe nicht numerisch ist.
										if (!IsNumeric (eingabe)) {
											invalidValue ();
										} else {
											nmUpdate = Convert.ToInt32 (eingabe);

											// Wert1 muss zwischen 0 und 250 sein.
											if (nmUpdate < 0 | nmUpdate > 250) {
												invalidValue ();
											} else {
												Console.WriteLine ("[Wert vermerkt]");
												Console.WriteLine ();
												break;
											}
										}
									}

									if (checkLocal == false) {
										Thread.Sleep (1000);
										Console.WriteLine ("Neue Temperatur:");
									}

									//checkLocal = false;
									while (checkLocal == false) {
										eingabe = Console.ReadLine ();

										// Modus verlassen und zurück zum Submenü.
										if (eingabe.ToLower () == "exit") {
											exit (2, ref checkMain, ref checkLocal);
											checkLocal = true;
											break;
										}

										// Wenn Eingabe nicht numerisch ist.
										if (!IsNumeric (eingabe)) {
											invalidValue ();
										} else {
											tpUpdate = Convert.ToInt32 (eingabe);

											// Wert1 muss zwischen -50 und 50 sein.
											if (tpUpdate < -50 | tpUpdate > 50) {
												invalidValue ();
											} else {
												Console.WriteLine ("[Wert vermerkt]");
												Console.WriteLine ();
												break;
											}
										}
									}

									if (checkLocal == false) {
										Thread.Sleep (1000);
										Console.WriteLine ("Neuer Luftdruck:");
									}

									//checkLocal = false;
									while (checkLocal == false) {
										eingabe = Console.ReadLine ();

										// Modus verlassen und zurück zum Submenü.
										if (eingabe.ToLower () == "exit") {
											exit (2, ref checkMain, ref checkLocal);
											checkLocal = true;
											break;
										}

										// Wenn Eingabe nicht numerisch ist.
										if (!IsNumeric (eingabe)) {
											invalidValue ();
										} else {
											ldUpdate = Convert.ToInt32 (eingabe);

											// Wert1 muss zwischen 1 und 10 sein.
											if (ldUpdate < 1 | ldUpdate > 10) {
												invalidValue ();
											} else {
												Console.WriteLine ("[Wert vermerkt]");
												Console.WriteLine ();
												break;
											}
										}
									}

									if (checkLocal == false) {
										// Einrückung bei negativer Temperatur.
										if (tpUpdate < 0)
											leerstelle = "";
										else
											leerstelle = " ";

										// Ausgabe der eingegeben (vermerkten) Werte. 
										Thread.Sleep (1500);
										Console.WriteLine ("Ihre neuen Werte für Tag " + tag + ":");
										Console.WriteLine ("---");
										Console.WriteLine ("Niederschlagsmenge: " + nmUpdate + " l/m²");
										Console.WriteLine ("Temperatur:        " + leerstelle + "" + tpUpdate + " °C");
										Console.WriteLine ("Luftdruck:          " + ldUpdate + " bar");
										Console.WriteLine ("---");
										Console.WriteLine ();

										tag--;
										Thread.Sleep (1500);
										Console.WriteLine ("Sollen die Werte gespeichert werden? [Ja/Nein]");
									}

									// while-Schleife zur Überprüfung, ob Werte gespeichert (in Array geschrieben)
									// werden sollen.
									while (checkLocal == false) {
										eingabe = Console.ReadLine ();

										if (eingabe.ToLower () == "ja" | eingabe.ToLower () == "j") {
											Console.WriteLine ();
											Console.WriteLine ("[Werte werden gespeichert]");
											Console.WriteLine ("Bitte warten.");

											nm [tag] = nmUpdate;
											tp [tag] = tpUpdate;
											ld [tag] = ldUpdate;

											Thread.Sleep (1500);
											Console.WriteLine ();
											Console.WriteLine ("[Werte gespeichert]");
											Thread.Sleep (1500);

											checkLocal = false;
											break;
										} else if (eingabe.ToLower () == "nein" | eingabe.ToLower () == "n") {
											Console.WriteLine ();
											Console.WriteLine ("[Abbruch: Werte werden nicht gespeichert]");
											Thread.Sleep (1500);

											checkLocal = false;
											break;
										} else {
											invalidValue ();
										}
									}

									checkLocal = false;

									break;

								case "2":

									exit (1, ref checkMain, ref checkLocal);

									break;

								case "exit":

									exit (1, ref checkMain, ref checkLocal);

									break;

								default:

									invalidValue ();
									consoleClear = false;

									break;
								}
							}

							break;

						// Neue Messdaten generieren.
						case "3":

							Console.WriteLine ();
							Console.WriteLine ("[Neue Messdaten werden generiert]");
							Console.WriteLine ("Bitte warten.");
							Thread.Sleep (3000);

							// Aufruf der Funktion, um neue Messdaten zu generieren.
							generateData (nm, tp, ld, niederschlagsmengeSpektrum, temperaturSpektrum);

							Console.WriteLine ("[Neue Messdaten generiert]");
							Thread.Sleep (1500);

							break;

						// Daten in Datei übernehmen.
						case "4":

							Console.WriteLine ();
							Console.WriteLine ("[Daten werden in aktuelle Datei übernommen]");
							Console.WriteLine ("Bitte warten.");
							Thread.Sleep (2500);

							saveDataToFile (nm, tp, ld, aktuelleDatei, sw);

							Console.WriteLine ("[Alle Daten in aktuelle Datei übernommen]");
							Thread.Sleep (1000);

							break;

						// Programm beenden.
						case "5":

							Thread.Sleep (1000);
							Console.WriteLine ();
							Console.WriteLine ("Möchten Sie das Programm wirklich beenden? [Ja/Nein]");
							Console.WriteLine ("Die aktuell gespeicherten / geänderten Daten werden");
							Console.WriteLine ("nicht in die Datei übernommen.");

							// while-Schleife zur Überprüfung der Eingabe (Ja oder Nein).
							checkLocal = false;
							while (checkLocal == false) {
								eingabe = Console.ReadLine ();

								if (eingabe.ToLower () == "ja" | eingabe.ToLower () == "j") {
									checkLocal = true;
									end ();
								} else if (eingabe.ToLower () == "nein" | eingabe.ToLower () == "n") {
									Console.WriteLine ();
									Console.WriteLine ("[Abbruch: Programm wird nicht beendet]");
									Thread.Sleep (1500);
									checkLocal = true;
								} else {
									invalidValue ();
								}
							}

							break;

						// Wechsel zur Dateiverwaltung.                                    
						case "6":

							//Console.WriteLine("[Daten werden in \""+ aktuelleDatei +"\" übernommen]");
							//saveDataToFile(nm, tp, ld, aktuelleDatei, sw);
							Console.WriteLine ("[Mögliche Änderungen werden verworfen]");
							Thread.Sleep (1500);

							exit (0, ref checkMain, ref checkLocal);

							break;

						// Wechsel zur Dateiverwaltung.
						case "exit":

							//Console.WriteLine("[Daten werden in \""+ aktuelleDatei +"\" übernommen]");
							//saveDataToFile(nm, tp, ld, aktuelleDatei, sw);
							Console.WriteLine ("[Mögliche Änderungen werden verworfen]");
							Thread.Sleep (1500);

							exit (0, ref checkMain, ref checkLocal);

							break;

						// Falls ungültiger Wert eingegeben wurde.
						default:

							invalidValue ();
							consoleClear = false;

							break;
						}
					}

					break;

				// Datei löschen.
				case "3":

					// Aufruf der Funktion, um alle existierenden Dateinamen auszugeben
					listAllFiles ();

					Console.WriteLine ();
					Thread.Sleep (500);
					Console.WriteLine ("Welche Datei möchten Sie löschen? [Index]");

					// Innere while-Schleife zur Fehlerüberprüfung (Index)
					checkLocal = false;
					while (checkLocal == false) {
						// Aufruf der Funktion, um alle existierenden Dateinamen in dateiNamen zu speichern
						dateiNamen = getAllFiles ();

						eingabe = Console.ReadLine ();

						// Modus verlassen und zurück zum Submenü.
						if (eingabe.ToLower () == "exit") {
							exit (2, ref checkMain, ref checkLocal);
							checkLocal = true;
							break;
						}

						// Wenn Eingabe nicht numerisch war
						if (!IsNumeric (eingabe)) {
							invalidValue ();
						} else {
							wert1 = Convert.ToInt32 (eingabe);

							// Wenn eingegebener Wert < 0 oder größer als Größe des Arrays dateiNamen war
							if (wert1 < 0 | wert1 >= dateiNamen.Length) {
								invalidValue ();
							} else {
								// Speichern des Namens der zu löschenden Datei in dateiZumLoeschen 
								dateiZumLoeschen = dateiNamen [wert1];

								Console.WriteLine ();
								Console.WriteLine ("Sind Sie sich sicher, dass Sie die Datei");
								Console.WriteLine ("\"" + dateiZumLoeschen + "\" löschen möchten? [Ja/Nein]");

								// while-Schleife zur Überprüfung der Eingabe (Ja oder Nein).
								checkLocal = false;
								while (checkLocal == false) {
									eingabe = Console.ReadLine ();

									if (eingabe.ToLower () == "ja" | eingabe.ToLower () == "j") {
										Console.WriteLine ();
										Console.WriteLine ("[Datei wird gelöscht]");

										// Aufruf der Funktion, um Datei zu löschen
										File.Delete ("..\\..\\..\\data\\" + dateiZumLoeschen + "");

										Thread.Sleep (500);
										Console.WriteLine ("[Datei gelöscht]");
										Thread.Sleep (1000);
										checkLocal = true;
									} else if (eingabe.ToLower () == "nein" | eingabe.ToLower () == "n") {
										Console.WriteLine ();
										Console.WriteLine ("[Datei wird nicht gelöscht]");
										Thread.Sleep (1000);
										checkLocal = true;
									} else {
										invalidValue ();
									}
								}
							}
						}
					}

					break;

				// Programm beenden (von Dateiverwaltung aus)
				case "4":

					Thread.Sleep (1000);
					Console.WriteLine ();
					Console.WriteLine ("Möchten Sie das Programm wirklich beenden? [Ja/Nein]");

					// while-Schleife zur Überprüfung der Eingabe (Ja oder Nein).
					checkLocal = false;
					while (checkLocal == false) {
						eingabe = Console.ReadLine ();

						if (eingabe.ToLower () == "ja" | eingabe.ToLower () == "j") {
							checkLocal = true;

							//Aufruf der Funktion, um Programm zu beenden
							end ();
						} else if (eingabe.ToLower () == "nein" | eingabe.ToLower () == "n") {
							Console.WriteLine ();
							Console.WriteLine ("[Abbruch: Programm wird nicht beendet]");
							Thread.Sleep (1500);
							checkLocal = true;
						} else {
							invalidValue ();
						}
					}

					break;

				// Wenn eingegegener Wert ungültig war
				default:

					invalidValue ();
					consoleClear = false;

					break;
				}
			}
		}

		// Hauptprogramm (Main) vorbei
		// Hier werden alle benutzten Funktion definiert/gelistet.


		/* Funktionalität:  Daten aus Array lesen und anzeigen
         * Parameter:       int min => Anfangstag von wo aus aus Arrays gelesen wird
         *                  int max => Endtag bis wohin aus Array gelesen wird
         *                  double[] niederschlag => Array mit allen Niederschlagswerten des Jahres
         *                  double [] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  int[] tagSpektrum => Spektrum, wie viele Tage/Monat
         * Rückgabewerte:   keine
        */
		public static void OutputDataFromArray (int min, int max, double[] niederschlag, double[] temperatur, double[] luftdruck, int[] tagSpektrum)
		{
			// Temporäre lokale Variablen.
			string leerstelle, datumString;
			int tag;
			min--;
			max--;

			// Schleife um alle Werte auszugeben.
			for (int i = min; i <= max; i++) {
				// Aktuellen Wert der Laufvariable "i" in Variable "tag" speichern.
				tag = i;

				// Zusammensetzung des Datums.
				datumString = getDateFromDay (tag, tagSpektrum);

				// Erhöhung von "tag" um 1, um aktuellen Tag zu erhalten.
				tag++;

				// Einrückung falls Temperatur negativ ist.
				if (temperatur [i] < 0)
					leerstelle = "";
				else
					leerstelle = " ";

				// Ausgabe der ausgelesenen Daten.
				Thread.Sleep (250);
				Console.WriteLine ("---");
				Console.WriteLine ("Tag " + tag + " - " + datumString + ":");
				Console.WriteLine ("Niederschlagsmenge: " + niederschlag [i] + " l/m²");
				Console.WriteLine ("Temperatur:        " + leerstelle + "" + temperatur [i] + " °C");
				Console.WriteLine ("Luftdruck:          " + luftdruck [i] + " bar");
				if (i == max)
					Console.WriteLine ("---");
			}
		}


		/* Funktionalität:  Mittelwert der Daten des angegebenen Zeitraums berechnen und anzeigen
         * Parameter:       int min => Anfangstag von wo aus aus Arrays gelesen wird
         *                  int max => Endtag bis wohin aus Array gelesen wird
         *                  double[] niederschlag => Array mit allen Niederschlagswerten des Jahres
         *                  double[] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  int[] tagSpektrum => Spektrum, wie viele Tage/Monat
         * Rückgabewerte:   keine
        */
		public static void OutputAverageFromArray (int min, int max, double[] niederschlag, double[] temperatur, double[] luftdruck, int[] tagSpektrum)
		{
			// Temporäre lokale Variablen.
			double summeNiederschlagsmenge = 0, summeTemperatur = 0, summeLuftdruck = 0;
			string leerstelle, datumStart, datumEnde;

			// Berechnung des zugehörigen Arrayindizes von eingegebenem Tag.
			min--;
			max--;

			// Summierung aller Daten aus angegebenem Zeitraum.
			for (int i = min; i <= max; i++) {
				summeNiederschlagsmenge += niederschlag [i];
				summeTemperatur += temperatur [i];
				summeLuftdruck += luftdruck [i];

			}

			// Ergebnisberechnung.
			summeNiederschlagsmenge = summeNiederschlagsmenge / (max - min);
			summeTemperatur = summeTemperatur / (max - min);
			summeLuftdruck = summeLuftdruck / (max - min);

			// Einrückung bei negativer Temperatur.
			if (summeTemperatur < 0)
				leerstelle = "";
			else
				leerstelle = " ";

			// Zusammensetzung des Datums.
			datumStart = getDateFromDay (min, tagSpektrum);
			datumEnde = getDateFromDay (max, tagSpektrum);

			// Ausgabe der Ergebnisse.
			Thread.Sleep (500);
			Console.WriteLine ();
			Console.WriteLine ("---");
			Console.WriteLine (datumStart + " - " + datumEnde + ":");
			Console.WriteLine ("Mittlere Niederschlagsmenge: " + Math.Round (summeNiederschlagsmenge, 2) + " l/m²");
			Console.WriteLine ("Mittlere Temperatur:        " + leerstelle + "" + Math.Round (summeTemperatur, 2) + " °C");
			Console.WriteLine ("Mittlerer Luftdruck:         " + Math.Round (summeLuftdruck, 2) + " bar");
			Console.WriteLine ("---");
		}


		/* Funktionalität:  Datum in Relation zum Tag erhalten
         * Parameter:       int t =>  Tag zu das Datum zusammengesetzt werden soll
         *                  int[] tagSpektrum => Spektrum, wie viele Tage/Monat
         * Rückgabewerte:   string datumString => Datum vom Datentyp string
        */
		public static string getDateFromDay (int tag, int[] tagSpektrum)
		{
			// Temporäre lokale Variablen.
			int summeMonat = 0, summeTag = 0, laufVariable = 0;
			string datumString, summeTagNull, summeMonatNull;

			// Berechnung und Zusammensetzung des Datums.
			while (summeTag <= tag - 31) {
				summeMonat++;
				summeTag += tagSpektrum [laufVariable];

				laufVariable++;
			}

			summeTag = tag - summeTag + 1;

			if (tagSpektrum [summeMonat] == 28 & summeTag == 29) {
				summeTag = 1;
				summeMonat++;
			}
			if (tagSpektrum [summeMonat] == 28 & summeTag == 30) {
				summeTag = 2;
				summeMonat++;
			}
			if (tagSpektrum [summeMonat] == 28 & summeTag == 31) {
				summeTag = 3;
				summeMonat++;
			}
			if (tagSpektrum [summeMonat] == 30 & summeTag == 31) {
				summeTag = 1;
				summeMonat++;
			}

			summeMonat++;

			// Überprüfung, ob Werte kleiner 10 sind, wenn ja füge ein Null hinzu.
			if (summeTag < 10)
				summeTagNull = "0";
			else
				summeTagNull = "";
			if (summeMonat < 10)
				summeMonatNull = "0";
			else
				summeMonatNull = "";

			// Zusammensetzung des Strings.
			datumString = (summeTagNull + summeTag + "." + summeMonatNull + summeMonat + ".");
			return datumString;
		}


		/* Funktionalität:  Tag in Relation zum Datum erhalten
         * Parameter:       string e =>  Eingegebener Tag
         *                  int[] tagSpektrum => Spektrum, wie viele Tage/Monat
         * Rückgabewerte:   int wert1 => Tag als Integer
        */
		public static int getDayValueFromDateString (string e, int[] tagSpektrum)
		{
			// Temporäre lokale Variablen.
			int wert1, wert2, temp;

			// Konvertierung in korrektes DateTime-Format, anschließend in String zurück.
			e = Convert.ToString (Convert.ToDateTime (e));

			// zusätzliche Anzahl Tage im Monat.
			wert2 = Convert.ToInt32 (e.Substring (0, 2));

			// Anzahl Monate.
			wert1 = Convert.ToInt32 (e.Substring (3, 2));
			temp = wert1;
			wert1 = 0;

			// Berechnung der Tage bis zum angegebenen Monat.
			for (int i = 0; i <= temp - 2; i++) {
				wert1 += tagSpektrum [i];
			}

			wert1 = wert1 + wert2;
			return wert1;
		}


		/* Funktionalität:  Anzeige, dass eingegebene Daten jetzt ausgewertet werden
         * Parameter:       keine
         * Rückgabewerte:   keine
        */
		public static void evaluation ()
		{
			Console.WriteLine ();
			Console.WriteLine ("[Auswertung wird vorgenommen]");
			Console.WriteLine ("Bitte warten.");
			Thread.Sleep (1000);
		}


		/* Funktionalität:  Generierung des Hauptmenüs
         * Parameter:       ref bool cc => true/false, ob Dateiverwaltung generiert werden soll oder nicht
         * Rückgabewerte:   keine
        */
		public static void generateDataAdministrationMenue (ref bool consoleClear)
		{
			// Menü
			if (consoleClear == true) {
				Console.Clear ();

				Thread.Sleep (500);
				Console.WriteLine ("Dateiverwaltung");
				Thread.Sleep (500);
				Console.WriteLine ("-------------------------");
				Console.WriteLine ("[1] Neue Datei anlegen");
				Console.WriteLine ("[2] Bestehende Datei auswählen");
				Console.WriteLine ("[3] Datei löschen");
				Console.WriteLine ("[4] Programm beenden");
				Console.WriteLine ("-------------------------");
			}

			consoleClear = true;
		}


		/* Funktionalität:  Generierung des Hauptmenüs
         * Parameter:       ref bool cc => true/false, ob Hauptmenü generiert werden soll oder nicht
         * 					string aktuelleDatei => enthält Namen der aktuell geladenen Datei
         * Rückgabewerte:   keine
        */
		public static void generateMainMenue (ref bool consoleClear, string aktuelleDatei)
		{
			// Menü
			if (consoleClear == true) {
				Console.Clear ();

				Thread.Sleep (500);
				Console.WriteLine ("Hauptmenü - " + aktuelleDatei + "");
				Thread.Sleep (500);
				Console.WriteLine ("-------------------------");
				Console.WriteLine ("[1] Daten abfragen");
				Console.WriteLine ("[2] Daten bearbeiten");
				Console.WriteLine ("[3] Neue Messdaten generieren");
				Console.WriteLine ("[4] Daten in Datei übernehmen");
				Console.WriteLine ("[5] Programm beenden");
				Console.WriteLine ("[6] Zurück");
				Console.WriteLine ("-------------------------");
				Console.WriteLine ("Dateiverwaltung -> Hauptmenü - " + aktuelleDatei + "");
			}

			consoleClear = true;
		}


		/* Funktionalität:  Generierung von Submenü 1 (Daten abfragen)
         * Parameter:       ref bool cc => true/false, ob Menü generiert werden soll oder nicht
         * 					string aktuelleDatei => enthält Namen der aktuell geladenen Datei
         * Rückgabewerte:   keine
        */
		public static void generateSubMenueDataSelect (ref bool consoleClear, string aktuelleDatei)
		{
			// Menü
			if (consoleClear == true) {
				Console.Clear ();

				Thread.Sleep (500);
				Console.WriteLine ("Untermenü (Daten abfragen)");
				Thread.Sleep (500);
				Console.WriteLine ("--------------------------");
				Console.WriteLine ("[1] Einzelner Tag");
				Console.WriteLine ("[2] Zeitraum");
				Console.WriteLine ("[3] Gesamtes Jahr");
				Console.WriteLine ("[4] Zurück");
				Console.WriteLine ("--------------------------");
				Console.WriteLine ("Dateiverwaltung -> Hauptmenü - " + aktuelleDatei + " -> [1] Daten abfragen");
			}

			consoleClear = true;
		}


		/* Funktionalität:  Generierung von Submenü 2 (Daten bearbeiten)
         * Parameter:       ref cc => true/false, ob Menü generiert werden soll oder nicht
         * 					string aktuelleDatei => enthält Namen der aktuell geladenen Datei
         * Rückgabewerte:   keine
         */
		public static void generateSubMenueDataUpdate (ref bool consoleClear, string aktuelleDatei)
		{
			// Menü
			if (consoleClear == true) {
				Console.Clear ();

				Thread.Sleep (500);
				Console.WriteLine ("Untermenü (Daten bearbeiten)");
				Thread.Sleep (500);
				Console.WriteLine ("--------------------------");
				Console.WriteLine ("[1] Einzelner Tag");
				Console.WriteLine ("[2] Zurück");
				Console.WriteLine ("--------------------------");
				Console.WriteLine ("Dateiverwaltung -> Hauptmenü - " + aktuelleDatei + " -> [2] Daten bearbeiten");
			}

			consoleClear = true;
		}


		/* Funktionalität:  Auflistung aller Dateinamen in der Konsole.
         * Parameter:       keine
         * Rückgabewerte:   keine
        */
		public static void listAllFiles ()
		{
			// Temporäre lokale Variablen.
			System.IO.DirectoryInfo ordner = new System.IO.DirectoryInfo ("..\\..\\..\\data");
			int dateiNummer = 0;
			string[] dateiNamen = new string[ordner.GetFiles ().Length];

			Console.WriteLine ();
			Console.WriteLine ("Bestehende Dateien:");
			Console.WriteLine ("---");

			// Schleife, um Dateien im Ordner durchzugehen.
			foreach (System.IO.FileInfo dateien in ordner.GetFiles()) {
				Console.WriteLine ("" + dateiNummer + ": " + dateien.Name);
				//dateiNamen[dateiNummer] = files.Name;
				dateiNummer++;
			}
			Console.WriteLine ("---");
		}


		/* Funktionalität:  Speichern aller Dateinamen in einer Variable (Array)
         * Parameter:       keine
         * Rückgabewerte:   string[] dateiNamen => Array mit allen Dateinamen
        */
		public static string[] getAllFiles ()
		{
			// Temporäre lokale Variablen.
			System.IO.DirectoryInfo ordner = new System.IO.DirectoryInfo ("..\\..\\..\\data");
			int dateiNummer = 0;
			string[] dateiNamen = new string[ordner.GetFiles ().Length];

			// Schleife, um Dateien im Ordner durchzugehen.
			foreach (System.IO.FileInfo dateien in ordner.GetFiles()) {
				dateiNamen [dateiNummer] = dateien.Name;
				dateiNummer++;
			}

			return dateiNamen;
		}


		public static void loadSrcDataFromFile (int[,] spektrum, StreamReader sr)
		{
			//sr = new StreamReader("..\\..\\..\\data\\src\\src.temperaturSpektrum.txt");
			string[] alleWerte = new string[12];
			string[] einzelneWerte = new string[2];

			// Schleife, um einzelne Werte in Arrays zu schreiben.
			for (int i = 0; i <= 11; i++) {
				alleWerte [i] = sr.ReadLine ();

				// zerschneiden des Strings in (zwei) einzelne Strings.
				einzelneWerte = alleWerte [i].Split (new Char[] { ',' });


				// einzelne Werte, konvertiert, in Array schreiben.
				spektrum [i, 0] = Convert.ToInt32 (einzelneWerte [0]);
				spektrum [i, 1] = Convert.ToInt32 (einzelneWerte [1]);
			}
		}


		/* Funktionalität:  Laden aller Daten einer bestimmten Datei
         * Parameter:       double[] niederschlagsmenge => Array mit allen Niederschlagswerten des Jahres
         *                  double[] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  string aktuelleDatei => betreffende Datei, aus welcher die Daten geladen werden
         *                  StreamReader sr => StreamReader-Objekt
         * Rückgabewerte:   keine
        */
		public static void LoadDataFromFile (double[] niederschlagsmenge, double[] temperatur, double[] luftdruck, string aktuelleDatei, StreamReader sr)
		{
			// Temporäre lokale Variablen.
			sr = new StreamReader ("..\\..\\..\\data\\" + aktuelleDatei + "");
			string[] alleWerteTag = new string[366];
			string[] einzelneWerteTag = new string[3];

			// Schleife, um einzelne Werte in Arrays zu schreiben.
			for (int i = 0; i <= 365; i++) {
				alleWerteTag [i] = sr.ReadLine ();
				// zerschneiden des Strings in (drei) einzelne Strings.
				einzelneWerteTag = alleWerteTag [i].Split (new Char[] { ',' });

				// einzelne Werte, konvertiert, in Array schreiben.
				niederschlagsmenge [i] = Convert.ToDouble (einzelneWerteTag [0]);
				temperatur [i] = Convert.ToDouble (einzelneWerteTag [1]);
				luftdruck [i] = Convert.ToDouble (einzelneWerteTag [2]);
			}

			sr.Close ();
		}


		/* Funktionalität:  Schreiben aller Daten (der Arrays) in bestimmte Datei
         * Parameter:       double[] niederschlagsmenge => Array mit allen Niederschlagswerten des Jahres
         *                  double[] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  string aktuelleDatei => betreffende Datei, aus welcher die Daten geladen werden
         *                  StreamReader sr => StreamReader-Objekt
         * 					StreamWriter sw => StreamWriter-Objekt
         * Rückgabewerte:   keine
        */
		public static void saveDataToFile (double[] niederschlagsmenge, double[] temperatur, double[] luftdruck, string aktuelleDatei, StreamWriter sw)
		{
			sw = new StreamWriter ("..\\..\\..\\data\\" + aktuelleDatei + "");

			// einzelne Werte aus Arrays in Dateischreiben.
			for (int i = 0; i <= 365; i++) {
				sw.WriteLine (niederschlagsmenge [i] + ", " + temperatur [i] + ", " + luftdruck [i]);
			}

			sw.Flush ();
			sw.Close ();
		}


		/* Funktionalität:  Zufällige Generierung aller Messdaten eines Jahres
         * Parameter:       double[] niederschlagsmenge => Array mit allen Niederschlagswerten des Jahres
         *                  double[] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  int[,] niederschlagsmengenSpektrum => Spektrum der verschiedenen Niederschlagsmengen
         *                  int[,] temperaturSpektrum => Spektrum der verschiedenen Temperaturwerte
         * Rückgabewerte:   keine
        */
		public static void generateData (double[] niederschlagsmenge, double[] temperatur, double[] luftdruck, int[,] niederschlagsmengeSpektrum, int[,] temperaturSpektrum)
		{
			// Temporäre lokale Variablen.
			var n = new Random ();
			int monatZaehler = 0;
			int tagZaehler = 30;

			// Initialisierung der verschiedenen Arrays mit zufälligen Werten. (Nm und Tp werden anschließend initialisiert).
			for (int i = 0; i <= 365; i++) {
				//nm[i] = n.Next(0, 100);
				//tp[i] = n.Next(-20, 40);
				luftdruck [i] = n.Next (1, 3);
			}

			// Initalisierung von Nm und Tp anhand vorher festgeschriebener Einschränkungen (Arrays vWNm & vWTp).
			for (int i = 0; i <= 365; i++) {

				if (i == tagZaehler & monatZaehler <= 10) {
					monatZaehler++;
					tagZaehler = tagZaehler + 30;
				}

				// generierte Werte werden in entsprechende Arrays geschrieben.
				niederschlagsmenge [i] = n.Next (niederschlagsmengeSpektrum [monatZaehler, 0], niederschlagsmengeSpektrum [monatZaehler, 1]);
				temperatur [i] = n.Next (temperaturSpektrum [monatZaehler, 0], temperaturSpektrum [monatZaehler, 1]);
			}
		}


		/* Funktionalität:  Tausch von Werten zweiter Integer-Variablen
         * Parameter:       ref int a => erster Wert
         *                  ref int b => zweiter Wert
         * Rückgabewerte:   keine
        */
		public static void swap (ref int a, ref int b)
		{
			// Temporäre lokale Variable(n).
			int temp;

			temp = a;
			a = b;
			b = temp;
		}


		/* Funktionalität:  Beendet das Programm/schließt die Konsole
         * Parameter:       keine
         * Rückgabewerte:   keine
         */
		public static void end ()
		{
			Thread.Sleep (1000);
			Console.WriteLine ();
			Console.WriteLine ("[Temporäre Daten werden gelöscht]");
			Thread.Sleep (1000);
			Console.WriteLine ("[Programm wird beendet]");
			Thread.Sleep (1500);

			Environment.Exit (0);
		}


		/* Funktionalität:  Ausgabe der allgemeinen Fehlermeldung
         * Parameter:       keine
         * Rückgabewerte:   keine
         */
		public static void invalidValue ()
		{
			Console.WriteLine ();
			Console.WriteLine ("[Fehler: Der eingegebene Wert ist ungültig]");
			Console.WriteLine ();
		}


		/* Funktionalität:  Ausgabe, ob nur Mittelwert oder ob auch alle Datensätze ausgegeben werden sollen
         * Parameter:       keine
         * Rückgabewerte:   keine
         */
		public static void averageOrAll ()
		{
			Thread.Sleep (500);
			Console.WriteLine ("Soll nur der Mittelwert ausgegeben werden oder sollen auch");
			Console.WriteLine ("alle Werte der Tage im angegebenen Zeitraum ausgegeben werden? [m/all]");
		}


		/* Funktionalität:  Exit-Funktionalität (um ins übergeordnete Menü zurück zu kommen)
         * Parameter:       int checkMenue => in welches Menü soll zurückgewechselt werden (statisch angegebener Wert)
         * 					ref bool checkMain => check-Variable für Hauptmenü-Schleife
         * 					ref bool checkLocal => check-Variable für alle "lokalen" inneren Schleifen
         * Rückgabewerte:   keine
         */
		public static void exit (int checkMenue, ref bool checkMain, ref bool checkLocal)
		{
			// zurück zur Dateiverwaltung.
			if (checkMenue == 0) {
				checkMain = true;
				Console.WriteLine ("[Wechsel zur Dateiverwaltung]");
				Thread.Sleep (1000);
			}
			// zurück zum Hauptmenü
			else if (checkMenue == 1) {
				checkLocal = true;
				Console.WriteLine ("[Wechsel zum Hauptmenü]");
				Thread.Sleep (1000);
			}
			// zurück zum aktuellen Submenü (bzw. Dateiverwaltung von Menüpunkt 1 oder 3 aus).
			else if (checkMenue == 2) {
				Console.WriteLine ("[Modus wird verlassen]");
				Thread.Sleep (1000);
			}
		}


		/* Funktionalität:  Funktion, um zu überprüfen, ob eine Eingabe Datumsformat hat.
         * Parameter:       string datumStr => eingegebener String
         *                  ref bool istDatumString => Wert legt fest, ob eingegebener Wert dem Datumsformat entspricht
         * Rückgabewerte:   bool true/false (ist Wert ein Datum ja/nein)
         */
		public static bool IsDate (string datumStr, ref bool istDatumString)
		{
			try {
				DateTime.Parse (datumStr);
				istDatumString = true;
				return true;
			} catch (System.FormatException) {
				istDatumString = false;
				return false;
			}
		}


		/* Funktionalität:  Funktion überprüft, ob Eingabe numerisch ist
         * Parameter:       string val => eingegebener Wert
         * Rückgabewerte:   bool true/false (ist Wert numerisch ja/nein)
         */
		public static bool IsNumeric (string val)
		{
			try {
				Int32.Parse (val);
				return true;
			} catch (System.FormatException) {
				return false;
			}
		}


		/* Funktionalität:  Funktion überprüft, Dateiname bereits existiert
         * Parameter:       string e => eingegebener Wert (Dateiname)
         * Rückgabewerte:   bool true/false (ist Datei vorhanden ja/nein)
         */
		public static bool fileExist (string e)
		{
			try {
				StreamReader sr = new StreamReader ("..\\..\\..\\data\\" + e + ".txt");
				sr.Close ();
				return false;
			} catch (Exception) {
				return true;
			}
		}


		/* Funktionalität:  Funktion überprüft, ob Dateiname zulässig ist (keine Sonderzeichen enthält)
         * Parameter:       string str => eingegebener Wert (Dateiname)
         * Rückgabewerte:   bool true/false (ist Name zulässig ja/nein)
         */
		public static bool IsValidFileName (string str)
		{
			System.Text.RegularExpressions.Regex pattern = new System.Text.RegularExpressions.Regex (@"^[A-Za-z0-9]+$");
			return pattern.IsMatch (str.Trim ());           
		}
	}
}
