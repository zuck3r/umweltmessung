﻿/*
 * ==== Code - Projekt Umweltmessung =======
 *
 * Autoren: Schumacher, Alex; Schwenk, Susanne (us, fia73)
 * Lehrkraft: Mango (ma)
 * Institution: Georg-Simon-Ohm Berufskolleg (GSO)
 * Beginn: 28.02.18, 10:36 Uhr
 * IDE: SharpDevelop / Microsoft Visual Studio
 * 
 * Aktuelle Version: 1.7
 * 
 * Release: 20.03.18, 12:05 Uhr
 * 
 * Beschreibung:
 * "Projekt - Umweltmessung" ist ein Programm, welches dem Anwender konsolenbasiert erlaubt,
 * automatisch generierte Messdaten (Niederschlagsmenge (nm), Temperatur (tp) und
 * Luftdruck (ld)) innerhalb eines Jahres verwalten zu können.
 * Dieser kann sich dabei Daten eines Tages, eines Zeitraums oder eines Jahres anzeigen lassen
 * und sich nach Wunsch den Mittelwert berechnen lassen.
 * Ebenso ist es möglich, die Daten eines einzelnen Tages zu bearbeiten und dann in den entsprechenden
 * Arrays zu speichern.
 * Neue Messdaten können ebenfalls aus der Anwendung heraus generiert werden.
 * 
 * ==================================================
 */
 
using System;
using System.Threading;
using System.IO;

namespace Umweltmessung
{
	class Program
	{
		// Hauptprogramm (Main) vorbei
        // Hier werden alle benutzten Funktion definiert/gelistet.

        
		/* Funktionalität:  Daten aus Array lesen und anzeigen
         * Parameter:       int min => Anfangstag von wo aus aus Arrays gelesen wird
         *                  int max => Endtag bis wohin aus Array gelesen wird
         *                  double[] niederschlag => Array mit allen Niederschlagswerten des Jahres
         *                  double [] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  int[] tagSpektrum => Spektrum, wie viele Tage/Monat
         * Rückgabewerte:   keine
        */
		public static void OutputDataFromArray(int min, int max, double[] niederschlag, double[] temperatur, double[] luftdruck, int[] tagSpektrum)
		{
            // Temporäre lokale Variablen.
            string leerstelle, datumString;
            int tag;
			min--;
			max--;
			
            // Schleife um alle Werte auszugeben.
			for(int i = min; i <= max; i++)
			{
                // Aktuellen Wert der Laufvariable "i" in Variable "tag" speichern.
				tag = i;
				
                // Zusammensetzung des Datums.
				datumString = getDateFromDay(tag, tagSpektrum);

                // Erhöhung von "tag" um 1, um aktuellen Tag zu erhalten.
				tag++;

                // Einrückung falls Temperatur negativ ist.
                if (temperatur[i] < 0) leerstelle = "";
                else leerstelle = " ";

                // Ausgabe der ausgelesenen Daten.
				Thread.Sleep(250);
				Console.WriteLine("---");
				Console.WriteLine("Tag "+ tag +" - "+ datumString +":");
				Console.WriteLine("Niederschlagsmenge: "+ niederschlag[i] +" l/m²");
				Console.WriteLine("Temperatur:        "+ leerstelle +""+ temperatur[i] +" °C");
				Console.WriteLine("Luftdruck:          "+ luftdruck[i] +" bar");
				if(i == max) Console.WriteLine("---");
			}
		}
		
		
        /* Funktionalität:  Mittelwert der Daten des angegebenen Zeitraums berechnen und anzeigen
         * Parameter:       int min => Anfangstag von wo aus aus Arrays gelesen wird
         *                  int max => Endtag bis wohin aus Array gelesen wird
         *                  double[] niederschlag => Array mit allen Niederschlagswerten des Jahres
         *                  double[] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  int[] tagSpektrum => Spektrum, wie viele Tage/Monat
         * Rückgabewerte:   keine
        */
		public static void OutputAverageFromArray(int min, int max, double[] niederschlag, double[] temperatur, double[] luftdruck, int[] tagSpektrum)
		{
            // Temporäre lokale Variablen.
			double summeNiederschlagsmenge = 0, summeTemperatur = 0, summeLuftdruck = 0;
            string leerstelle, datumStart, datumEnde;
			
            // Berechnung des zugehörigen Arrayindizes von eingegebenem Tag.
			min--;
			max--;

            // Summierung aller Daten aus angegebenem Zeitraum.
			for(int i = min; i <= max; i++)
			{
				summeNiederschlagsmenge += niederschlag[i];
				summeTemperatur += temperatur[i];
				summeLuftdruck += luftdruck[i];
				
			}
			
            // Ergebnisberechnung.
			summeNiederschlagsmenge = summeNiederschlagsmenge / (max - min);
			summeTemperatur = summeTemperatur / (max - min);
			summeLuftdruck = summeLuftdruck / (max - min);

            // Einrückung bei negativer Temperatur.
            if (summeTemperatur < 0) leerstelle = "";
            else leerstelle = " ";
            
            // Zusammensetzung des Datums.
            datumStart = getDateFromDay(min, tagSpektrum);
            datumEnde = getDateFromDay(max, tagSpektrum);
            
            // Ausgabe der Ergebnisse.
			Thread.Sleep(500);
			Console.WriteLine();
			Console.WriteLine("---");
			Console.WriteLine(datumStart +" - "+ datumEnde +":");
			Console.WriteLine("Mittlere Niederschlagsmenge: "+ Math.Round(summeNiederschlagsmenge, 2) +" l/m²");
			Console.WriteLine("Mittlere Temperatur:        "+ leerstelle +""+ Math.Round(summeTemperatur, 2) +" °C");
			Console.WriteLine("Mittlerer Luftdruck:         "+ Math.Round(summeLuftdruck, 2) +" bar");
			Console.WriteLine("---");
		}

		
        /* Funktionalität:  Datum in Relation zum Tag erhalten
         * Parameter:       int t =>  Tag zu das Datum zusammengesetzt werden soll
         *                  int[] tagSpektrum => Spektrum, wie viele Tage/Monat
         * Rückgabewerte:   string datumString => Datum vom Datentyp string
        */
		public static string getDateFromDay(int tag, int[] tagSpektrum)
		{
            // Temporäre lokale Variablen.
			int summeMonat = 0, summeTag = 0, laufVariable = 0;
			string datumString, summeTagNull, summeMonatNull;
			
            // Berechnung und Zusammensetzung des Datums.
			while(summeTag <= tag-31)
				{
					summeMonat++;
					summeTag += tagSpektrum[laufVariable];
					
					laufVariable++;
				}
				
				summeTag = tag - summeTag + 1;
				
				if(tagSpektrum[summeMonat] == 28 & summeTag == 29) { summeTag = 1; summeMonat++; }
				if(tagSpektrum[summeMonat] == 28 & summeTag == 30) { summeTag = 2; summeMonat++; }
				if(tagSpektrum[summeMonat] == 28 & summeTag == 31) { summeTag = 3; summeMonat++; }
				if(tagSpektrum[summeMonat] == 30 & summeTag == 31) { summeTag = 1; summeMonat++; }
				
				summeMonat++;
				
                // Überprüfung, ob Werte kleiner 10 sind, wenn ja füge ein Null hinzu.
				if(summeTag < 10) summeTagNull = "0";
				else summeTagNull = "";
				if(summeMonat < 10) summeMonatNull = "0";
				else summeMonatNull = "";
				
                // Zusammensetzung des Strings.
				datumString = (summeTagNull + summeTag +"."+ summeMonatNull + summeMonat +".");
				return datumString;
		}


        /* Funktionalität:  Tag in Relation zum Datum erhalten
         * Parameter:       string e =>  Eingegebener Tag
         *                  int[] tagSpektrum => Spektrum, wie viele Tage/Monat
         * Rückgabewerte:   int wert1 => Tag als Integer
        */
		public static int getDayValueFromDateString(string e, int[] tagSpektrum)
		{
            // Temporäre lokale Variablen.
			int wert1, wert2, temp;
			
            // Konvertierung in korrektes DateTime-Format, anschließend in String zurück.
			e = Convert.ToString(Convert.ToDateTime(e));

			// zusätzliche Anzahl Tage im Monat.
			wert2 = Convert.ToInt32(e.Substring(0, 2));
												
			// Anzahl Monate.
			wert1 = Convert.ToInt32(e.Substring(3, 2));
			temp = wert1;
			wert1 = 0;
            
            // Berechnung der Tage bis zum angegebenen Monat.
			for(int i = 0; i <= temp-2; i++)
			{
				wert1 += tagSpektrum[i];
			}
			
            wert1 = wert1 + wert2;
			return wert1;
		}
		
		
        /* Funktionalität:  Anzeige, dass eingegebene Daten jetzt ausgewertet werden
         * Parameter:       keine
         * Rückgabewerte:   keine
        */
		public static void evaluation()
		{
			Console.WriteLine();
			Console.WriteLine("[Auswertung wird vorgenommen]");
			Console.WriteLine("Bitte warten.");
			Thread.Sleep(1000);
		}

		
        /* Funktionalität:  Generierung des Hauptmenüs
         * Parameter:       ref bool cc => true/false, ob Dateiverwaltung generiert werden soll oder nicht
         * Rückgabewerte:   keine
        */
        public static void generateDataAdministrationMenue(ref bool consoleClear)
        {
            // Menü
            if (consoleClear == true)
            {
                Console.Clear();

                Thread.Sleep(500);
                Console.WriteLine("Dateiverwaltung");
                Thread.Sleep(500);
                Console.WriteLine("-------------------------");
                Console.WriteLine("[1] Neue Datei anlegen");
                Console.WriteLine("[2] Bestehende Datei auswählen");
                Console.WriteLine("[3] Datei löschen");
                Console.WriteLine("[4] Programm beenden");
                Console.WriteLine("-------------------------");
            }

            consoleClear = true;
        }
        

        /* Funktionalität:  Generierung des Hauptmenüs
         * Parameter:       ref bool cc => true/false, ob Hauptmenü generiert werden soll oder nicht
         * 					string aktuelleDatei => enthält Namen der aktuell geladenen Datei
         * Rückgabewerte:   keine
        */
		public static void generateMainMenue(ref bool consoleClear, string aktuelleDatei)
		{
			// Menü
			if(consoleClear == true)
			{
				Console.Clear();

				Thread.Sleep(500);
				Console.WriteLine("Hauptmenü - "+ aktuelleDatei +"");
				Thread.Sleep(500);
				Console.WriteLine("-------------------------");
				Console.WriteLine("[1] Daten abfragen");
				Console.WriteLine("[2] Daten bearbeiten");
				Console.WriteLine("[3] Neue Messdaten generieren");
				Console.WriteLine("[4] Daten in Datei übernehmen");
				Console.WriteLine("[5] Programm beenden");
				Console.WriteLine("[6] Zurück");
				Console.WriteLine("-------------------------");
				Console.WriteLine("Dateiverwaltung -> Hauptmenü - "+ aktuelleDatei +"");
			}

			consoleClear = true;
		}
		
		
        /* Funktionalität:  Generierung von Submenü 1 (Daten abfragen)
         * Parameter:       ref bool cc => true/false, ob Menü generiert werden soll oder nicht
         * 					string aktuelleDatei => enthält Namen der aktuell geladenen Datei
         * Rückgabewerte:   keine
        */
		public static void generateSubMenueDataSelect(ref bool consoleClear, string aktuelleDatei)
		{
			// Menü
			if(consoleClear == true)
			{
				Console.Clear();

				Thread.Sleep(500);
				Console.WriteLine("Untermenü (Daten abfragen)");
				Thread.Sleep(500);
				Console.WriteLine("--------------------------");
				Console.WriteLine("[1] Einzelner Tag");
				Console.WriteLine("[2] Zeitraum");
				Console.WriteLine("[3] Gesamtes Jahr");
				Console.WriteLine("[4] Zurück");
				Console.WriteLine("--------------------------");
				Console.WriteLine("Dateiverwaltung -> Hauptmenü - "+ aktuelleDatei +" -> [1] Daten abfragen");
			}
			
			consoleClear = true;
		}
		
		
        /* Funktionalität:  Generierung von Submenü 2 (Daten bearbeiten)
         * Parameter:       ref cc => true/false, ob Menü generiert werden soll oder nicht
         * 					string aktuelleDatei => enthält Namen der aktuell geladenen Datei
         * Rückgabewerte:   keine
         */
		public static void generateSubMenueDataUpdate(ref bool consoleClear, string aktuelleDatei)
		{
			// Menü
			if(consoleClear == true)
			{
				Console.Clear();

				Thread.Sleep(500);
				Console.WriteLine("Untermenü (Daten bearbeiten)");
				Thread.Sleep(500);
				Console.WriteLine("--------------------------");
				Console.WriteLine("[1] Einzelner Tag");
				Console.WriteLine("[2] Zurück");
				Console.WriteLine("--------------------------");
				Console.WriteLine("Dateiverwaltung -> Hauptmenü - "+ aktuelleDatei +" -> [2] Daten bearbeiten");
			}
			
			consoleClear = true;
		}
		
		
		/* Funktionalität:  Auflistung aller Dateinamen in der Konsole.
         * Parameter:       keine
         * Rückgabewerte:   keine
        */
        public static void listAllFiles()
		{
        	// Temporäre lokale Variablen.
			System.IO.DirectoryInfo ordner = new System.IO.DirectoryInfo("..\\..\\..\\data");
            int dateiNummer = 0;
            string[] dateiNamen = new string[ordner.GetFiles().Length];
            
            Console.WriteLine();
            Console.WriteLine("Bestehende Dateien:");
            Console.WriteLine("---");
            
            // Schleife, um Dateien im Ordner durchzugehen.
            foreach (System.IO.FileInfo dateien in ordner.GetFiles())
            {
            	Console.WriteLine(""+ dateiNummer +": " + dateien.Name);
                //dateiNamen[dateiNummer] = files.Name;
                dateiNummer++;
            }
            Console.WriteLine("---");
        }
        
        
        /* Funktionalität:  Speichern aller Dateinamen in einer Variable (Array)
         * Parameter:       keine
         * Rückgabewerte:   string[] dateiNamen => Array mit allen Dateinamen
        */
        public static string[] getAllFiles()
        {
        	// Temporäre lokale Variablen.
        	System.IO.DirectoryInfo ordner = new System.IO.DirectoryInfo("..\\..\\..\\data");
            int dateiNummer = 0;
            string[] dateiNamen = new string[ordner.GetFiles().Length];
        	
            // Schleife, um Dateien im Ordner durchzugehen.
        	foreach (System.IO.FileInfo dateien in ordner.GetFiles())
            {
                dateiNamen[dateiNummer] = dateien.Name;
                dateiNummer++;
            }
        	
        	return dateiNamen;
        }
        
        
        public static void loadSrcDataFromFile(int[,] spektrum, StreamReader sr)
        {
        	//sr = new StreamReader("..\\..\\..\\data\\src\\src.temperaturSpektrum.txt");
            string[] alleWerte = new string[12];
            string[] einzelneWerte = new string[2];
            
            // Schleife, um einzelne Werte in Arrays zu schreiben.
            for(int i = 0; i <= 11; i++)
            {
            	alleWerte[i] = sr.ReadLine();
            	
            	// zerschneiden des Strings in (zwei) einzelne Strings.
            	einzelneWerte = alleWerte[i].Split(new Char[] { ',' });

                
                // einzelne Werte, konvertiert, in Array schreiben.
                spektrum[i, 0] = Convert.ToInt32(einzelneWerte[0]);
                spektrum[i, 1] = Convert.ToInt32(einzelneWerte[1]);
            }
        }
        
        
        /* Funktionalität:  Laden aller Daten einer bestimmten Datei
         * Parameter:       double[] niederschlagsmenge => Array mit allen Niederschlagswerten des Jahres
         *                  double[] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  string aktuelleDatei => betreffende Datei, aus welcher die Daten geladen werden
         *                  StreamReader sr => StreamReader-Objekt
         * Rückgabewerte:   keine
        */
        public static void LoadDataFromFile(double[] niederschlagsmenge, double[] temperatur, double[] luftdruck, string aktuelleDatei, StreamReader sr)
        {
        	// Temporäre lokale Variablen.
        	sr = new StreamReader("..\\..\\..\\data\\"+ aktuelleDatei +"");
            string[] alleWerteTag = new string[366];
            string[] einzelneWerteTag = new string[3];
            
            // Schleife, um einzelne Werte in Arrays zu schreiben.
            for(int i = 0; i <= 365; i++)
            {
            	alleWerteTag[i] = sr.ReadLine();
            	// zerschneiden des Strings in (drei) einzelne Strings.
                einzelneWerteTag = alleWerteTag[i].Split(new Char[] { ',' });
                
                // einzelne Werte, konvertiert, in Array schreiben.
                niederschlagsmenge[i] = Convert.ToDouble(einzelneWerteTag[0]);
                temperatur[i] = Convert.ToDouble(einzelneWerteTag[1]);
                luftdruck[i] = Convert.ToDouble(einzelneWerteTag[2]);
            }
            
            sr.Close();
        }
        
        
        /* Funktionalität:  Schreiben aller Daten (der Arrays) in bestimmte Datei
         * Parameter:       double[] niederschlagsmenge => Array mit allen Niederschlagswerten des Jahres
         *                  double[] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  string aktuelleDatei => betreffende Datei, aus welcher die Daten geladen werden
         *                  StreamReader sr => StreamReader-Objekt
         * 					StreamWriter sw => StreamWriter-Objekt
         * Rückgabewerte:   keine
        */
        public static void saveDataToFile(double[] niederschlagsmenge, double[] temperatur, double[] luftdruck, string aktuelleDatei, StreamWriter sw)
        {
            sw = new StreamWriter("..\\..\\..\\data\\"+ aktuelleDatei +"");
            
            // einzelne Werte aus Arrays in Dateischreiben.
            for (int i = 0; i <= 365; i++)
            {
            	sw.WriteLine(niederschlagsmenge[i] +", "+ temperatur[i] +", "+ luftdruck[i]);
            }
                         		    
            sw.Flush();
            sw.Close();
        }
       	
        
        /* Funktionalität:  Zufällige Generierung aller Messdaten eines Jahres
         * Parameter:       double[] niederschlagsmenge => Array mit allen Niederschlagswerten des Jahres
         *                  double[] temperatur => Array mit allen Temperaturwerten des Jahres
         *                  double[] luftdruck => Array mit allen Luftdruckwerten des Jahres
         *                  int[,] niederschlagsmengenSpektrum => Spektrum der verschiedenen Niederschlagsmengen
         *                  int[,] temperaturSpektrum => Spektrum der verschiedenen Temperaturwerte
         * Rückgabewerte:   keine
        */
		public static void generateData(double[] niederschlagsmenge, double[] temperatur, double[] luftdruck, int[,] niederschlagsmengeSpektrum, int[,] temperaturSpektrum)
		{
            // Temporäre lokale Variablen.
			var n = new Random();
            int monatZaehler = 0;
            int tagZaehler = 30;
			
			// Initialisierung der verschiedenen Arrays mit zufälligen Werten. (Nm und Tp werden anschließend initialisiert).
			for(int i = 0; i <= 365; i++)
			{
				//nm[i] = n.Next(0, 100);
				//tp[i] = n.Next(-20, 40);
				luftdruck[i] = n.Next(1, 3);
			}
			
			// Initalisierung von Nm und Tp anhand vorher festgeschriebener Einschränkungen (Arrays vWNm & vWTp).
			for(int i = 0; i <= 365; i++)
			{
				
				if(i == tagZaehler & monatZaehler <= 10)
				{
					monatZaehler++;
					tagZaehler = tagZaehler + 30;
				}
				
                // generierte Werte werden in entsprechende Arrays geschrieben.
				niederschlagsmenge[i] = n.Next(niederschlagsmengeSpektrum[monatZaehler, 0], niederschlagsmengeSpektrum[monatZaehler, 1]);
				temperatur[i] = n.Next(temperaturSpektrum[monatZaehler, 0], temperaturSpektrum[monatZaehler, 1]);
			}
		}
		
		
        /* Funktionalität:  Tausch von Werten zweiter Integer-Variablen
         * Parameter:       ref int a => erster Wert
         *                  ref int b => zweiter Wert
         * Rückgabewerte:   keine
        */
        public static void swap(ref int a, ref int b)
        {
        	// Temporäre lokale Variable(n).
            int temp;

            temp = a;
            a = b;
            b = temp;
        }
        
        
        /* Funktionalität:  Beendet das Programm/schließt die Konsole
         * Parameter:       keine
         * Rückgabewerte:   keine
         */
		public static void end()
		{
			Thread.Sleep(1000);
			Console.WriteLine();
			Console.WriteLine("[Temporäre Daten werden gelöscht]");
			Thread.Sleep(1000);
			Console.WriteLine("[Programm wird beendet]");
			Thread.Sleep(1500);
			
			Environment.Exit(0);
		}
		
		
        /* Funktionalität:  Ausgabe der allgemeinen Fehlermeldung
         * Parameter:       keine
         * Rückgabewerte:   keine
         */
		public static void invalidValue()
		{
			Console.WriteLine();
			Console.WriteLine("[Fehler: Der eingegebene Wert ist ungültig]");
			Console.WriteLine();
		}
		
		
        /* Funktionalität:  Ausgabe, ob nur Mittelwert oder ob auch alle Datensätze ausgegeben werden sollen
         * Parameter:       keine
         * Rückgabewerte:   keine
         */
        public static void averageOrAll()
        {
            Thread.Sleep(500);
            Console.WriteLine("Soll nur der Mittelwert ausgegeben werden oder sollen auch");
            Console.WriteLine("alle Werte der Tage im angegebenen Zeitraum ausgegeben werden? [m/all]");
        }
        
        
        /* Funktionalität:  Exit-Funktionalität (um ins übergeordnete Menü zurück zu kommen)
         * Parameter:       int checkMenue => in welches Menü soll zurückgewechselt werden (statisch angegebener Wert)
         * 					ref bool checkMain => check-Variable für Hauptmenü-Schleife
         * 					ref bool checkLocal => check-Variable für alle "lokalen" inneren Schleifen
         * Rückgabewerte:   keine
         */
        public static void exit(int checkMenue, ref bool checkMain, ref bool checkLocal)
        {
            // zurück zur Dateiverwaltung.
            if (checkMenue == 0)
            {
                checkMain = true;
                Console.WriteLine("[Wechsel zur Dateiverwaltung]");
                Thread.Sleep(1000);
            }
            // zurück zum Hauptmenü
            else if (checkMenue == 1)
            {
                checkLocal = true;
                Console.WriteLine("[Wechsel zum Hauptmenü]");
                Thread.Sleep(1000);
            }
            // zurück zum aktuellen Submenü (bzw. Dateiverwaltung von Menüpunkt 1 oder 3 aus).
            else if (checkMenue == 2)
            {
            	Console.WriteLine("[Modus wird verlassen]");
                Thread.Sleep(1000);
            }
        } 
		
        
        /* Funktionalität:  Funktion, um zu überprüfen, ob eine Eingabe Datumsformat hat.
         * Parameter:       string datumStr => eingegebener String
         *                  ref bool istDatumString => Wert legt fest, ob eingegebener Wert dem Datumsformat entspricht
         * Rückgabewerte:   bool true/false (ist Wert ein Datum ja/nein)
         */
        public static bool IsDate(string datumStr, ref bool istDatumString)
        {
			try
			{
				DateTime.Parse(datumStr);
				istDatumString = true;
				return true;
			}
			catch(System.FormatException)
			{
				istDatumString = false;
				return false;
			}
        }
        
        
        /* Funktionalität:  Funktion überprüft, ob Eingabe numerisch ist
         * Parameter:       string val => eingegebener Wert
         * Rückgabewerte:   bool true/false (ist Wert numerisch ja/nein)
         */
		public static bool IsNumeric(string val)
		{
			try
    		{
        		Int32.Parse(val);
        		return true;
    		}
    		catch (System.FormatException)
    		{
        		return false;
    		}
		}
		
		
        /* Funktionalität:  Funktion überprüft, Dateiname bereits existiert
         * Parameter:       string e => eingegebener Wert (Dateiname)
         * Rückgabewerte:   bool true/false (ist Datei vorhanden ja/nein)
         */
        public static bool fileExist(string e)
        {
            try
            {
                StreamReader sr = new StreamReader("..\\..\\..\\data\\" + e + ".txt");
                sr.Close();
                return false;
	        }
            catch (Exception)
            {
                return true;
            }
        }
        
        
        /* Funktionalität:  Funktion überprüft, ob Dateiname zulässig ist (keine Sonderzeichen enthält)
         * Parameter:       string str => eingegebener Wert (Dateiname)
         * Rückgabewerte:   bool true/false (ist Name zulässig ja/nein)
         */
        public static bool IsValidFileName(string str)
		{
    		System.Text.RegularExpressions.Regex pattern = new System.Text.RegularExpressions.Regex(@"^[A-Za-z0-9]+$");
    		return pattern.IsMatch(str.Trim());           
		}
	}
}