﻿/*
 * ==== Code - Projekt Umweltmessung =======
 *
 * Autoren: S., Alex; S., Susanne (us, fia73)
 * Lehrkraft: (ma)
 * Institution: (GSO)
 * Beginn: 28.02.18, 10:36 Uhr
 * IDE: SharpDevelop / Microsoft Visual Studio / MonoDevelop
 * 
 * Aktuelle Version: 1.7.1
 * 
 * Release: 20.03.18, 12:05 Uhr
 * 
 * Beschreibung:
 * "Projekt - Umweltmessung" ist ein Programm, welches dem Anwender konsolenbasiert erlaubt,
 * automatisch generierte Messdaten (Niederschlagsmenge (nm), Temperatur (tp) und
 * Luftdruck (ld)) innerhalb eines Jahres verwalten zu können.
 *
 * Dieser kann sich dabei Daten eines Tages, eines Zeitraums oder eines Jahres anzeigen lassen
 * und sich nach Wunsch den Mittelwert berechnen lassen.
 * Ebenso ist es möglich, die Daten eines einzelnen Tages zu bearbeiten und dann in den entsprechenden
 * Arrays zu speichern.
 *
 * Neue Messdaten können ebenfalls aus der Anwendung heraus generiert werden.
 * 
 * Letztendlich ist es auch möglich, die Daten in eine entsprechende Datei zu speichern, die Daten von
 * dort wieder zu laden und anwendungsintern zu bearbeiten.
 * 
 * ==================================================
 */